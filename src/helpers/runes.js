const config = require('../config/config.json');
const runes = require(config.infos.rune);

module.exports = class Runes {
    static placeRunes(image, runes, x, y) {
        runes[0].resize(51, 51);
        image.blit(runes[0], 13 + x, 44 + y);

        for (let i = 1; i < 6; i++) {
            runes[i].resize(31, 31);
        }

        image.blit(runes[1], 23 + x, 97 + y);
        image.blit(runes[2], 23 + x, 140 + y);
        image.blit(runes[3], 23 + x, 183 + y);
        image.blit(runes[4], 67 + x, 54 + y);
        image.blit(runes[5], 67 + x, 97 + y);
    }

    static placeRuneSlots(image, slot, x, y) {
        image.blit(slot, 21 + x, 52 + y);
        image.blit(slot, 21 + x, 95 + y);
        image.blit(slot, 21 + x, 138 + y);
        image.blit(slot, 21 + x, 181 + y);
        image.blit(slot, 65 + x, 52 + y);
        image.blit(slot, 65 + x, 95 + y);
    }

    getRunes(summoner, styles) {
        let promises = [];

        for (let i = 0; i < 6; i++) {
            promises.push(new Promise(function (resolve) {
                styles[i < 4 ? 0 : 1].slots.forEach(function (slots) {
                    slots.runes.forEach(function (runes) {
                        if (+runes.id === summoner.perks.perkIds[i]) {
                            resolve(runes);
                        }
                    });
                });
            }));
        }

        return Promise.all(promises);
    }

    getRuneStyleInfos(summoner) {
        return Promise.all(
            [
                this.getRuneStyleInfo(summoner, 'perkStyle'),
                this.getRuneStyleInfo(summoner, 'perkSubStyle')
            ]
        )
    }

    getRuneStyleInfo(summoner, style) {
        return new Promise(function (resolve) {
            runes.forEach(function (element) {
                if (+element.id === summoner.perks[style]) {
                    resolve(element);
                }
            });
        });
    }
};