const Jimp          = require('jimp');
const config        = require('../config/config.json');
const summonerIcons = require(config.infos.profileicon);

module.exports = class SummonerIconService {
    static getSummonerIconSlot(dataKey) {
        return Jimp.read("./src/" + config.images.icon + summonerIcons.data[dataKey].image.full);
    }

    static loadSummonerIcon(summoner) {
        if (summoner.summonerLevel < 30) {
            return Jimp.read("./src/assets/img/lvl/" + 1 + ".png");
        }
        if (summoner.summonerLevel < 50) {
            return Jimp.read("./src/assets/img/lvl/" + 30 + ".png");
        }

        return Jimp.read("./src/assets/img/lvl/" + (summoner.summonerLevel - (summoner.summonerLevel % 25) + ".png"));
    }
};