const Jimp   = require('jimp');
const config = require('../config/config.json');

module.exports = class RunesService {
    static loadRunes(runes) {
        let promises = [];

        for (let i = 0; i < 6; i++)
            promises.push(Jimp.read('./src/' + config.images.rune + runes[i].icon));

        return Promise.all(promises);
    }
};