const FontsService = require('../services/fonts.service');
const Jimp         = require('jimp');
const Converter    = require('./converter');

module.exports = class Fonts {
    font12;
    font14;
    font14Gold;
    font16;
    font16Gold;
    converter;

    constructor() {
        FontsService.loadAllFonts().then(fonts => this.setFonts(fonts));
        this.converter = new Converter();
    }

    setFonts(fonts) {
        this.font12     = fonts[0];
        this.font14     = fonts[1];
        this.font14Gold = fonts[2];
        this.font16     = fonts[3];
        this.font16Gold = fonts[4];
    }

    setupText(summoner, image, x, y) {
        image.print(
            this.font16,
            19 + x,
            17 + y,
            {
                text      : summoner.summonerName,
                alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
                alignmentY: Jimp.VERTICAL_ALIGN_MIDDLE
            },
            192,
            21
        );

        image.print(this.font16Gold, 33 + x, 300 + y, "Solo/Duo");
        image.print(this.font16Gold, 138 + x, 300 + y, "Flex 5v5");

        image.print(this.font14Gold, 99 + x, 364 + y, "Wins");
        image.print(this.font14Gold, 96 + x, 388 + y, "Losses");

        if (summoner.ranks["RANKED_SOLO_5x5"].tier !== "") {
            image.print(this.font14, 33 + x, 332 + y, summoner.ranks["RANKED_SOLO_5x5"].tier.charAt(0) + Converter.roman_to_Int(summoner.ranks["RANKED_SOLO_5x5"].rank) + " " + summoner.ranks["RANKED_SOLO_5x5"].leaguePoints + "LP");
            image.print(this.font14, 33 + x, 364 + y, summoner.ranks["RANKED_SOLO_5x5"].wins + "");
            image.print(this.font14, 33 + x, 388 + y, summoner.ranks["RANKED_SOLO_5x5"].losses + "");
            image.print(this.font14, 51 + x, 418 + y, Math.round(summoner.ranks["RANKED_SOLO_5x5"].wins / (summoner.ranks["RANKED_SOLO_5x5"].wins + summoner.ranks["RANKED_SOLO_5x5"].losses) * 100) + "%");
        } else {
            image.print(this.font14, 33 + x, 332 + y, "Unranked");
        }

        if (summoner.ranks["RANKED_FLEX_SR"].tier !== "") {
            image.print(this.font14, 151 + x, 418 + y, Math.round(summoner.ranks["RANKED_FLEX_SR"].wins / (summoner.ranks["RANKED_FLEX_SR"].wins + summoner.ranks["RANKED_FLEX_SR"].losses) * 100) + "%");
            image.print(
                this.font14,
                143 + x,
                388 + y,
                {
                    text      : summoner.ranks["RANKED_FLEX_SR"].losses + "",
                    alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
                    alignmentY: Jimp.VERTICAL_ALIGN_TOP
                },
                55,
                18
            );
            image.print(
                this.font14,
                129 + x,
                332 + y,
                {
                    text      : summoner.ranks["RANKED_FLEX_SR"].tier.charAt(0) + Converter.roman_to_Int(summoner.ranks["RANKED_FLEX_SR"].rank) + " " + summoner.ranks["RANKED_FLEX_SR"].leaguePoints + "LP",
                    alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
                    alignmentY: Jimp.VERTICAL_ALIGN_TOP
                },
                66,
                18
            );
            image.print(
                this.font14,
                143 + x,
                364 + y,
                {
                    text      : summoner.ranks["RANKED_FLEX_SR"].wins + "",
                    alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
                    alignmentY: Jimp.VERTICAL_ALIGN_TOP
                },
                55,
                18
            );
        } else {
            image.print(
                this.font14,
                131 + x,
                332 + y,
                {
                    text      : "Unranked",
                    alignmentX: Jimp.HORIZONTAL_ALIGN_RIGHT,
                    alignmentY: Jimp.VERTICAL_ALIGN_TOP
                },
                66,
                18
            );
        }

        image.print(
            this.font12,
            102 + x,
            273 + y,
            {
                text      : summoner.summonerLevel + "",
                alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
                alignmentY: Jimp.VERTICAL_ALIGN_TOP
            },
            26,
            15
        );

        if (summoner.mastery !== undefined)
            image.print(
                this.font14,
                143 + x,
                50 + y,
                {
                    text      : summoner.mastery.championPoints + "",
                    alignmentX: Jimp.HORIZONTAL_ALIGN_CENTER,
                    alignmentY: Jimp.VERTICAL_ALIGN_TOP
                },
                63,
                18
            );
    }
};