const config        = require('../config/config.json');
const summonerIcons = require(config.infos.profileicon);

module.exports = class SummonerIcons {
    getSummonerIcon(summoner) {
        return new Promise(function (resolve) {
            for (let dataKey in summonerIcons.data)
                if (+summonerIcons.data[dataKey].id === summoner.profileIconId)
                    resolve(dataKey);
        });
    }

    static placeSummonerIcon(image, icon, x, y) {
        icon.resize(146, 146);
        image.blit(icon, 42 + x, 164 + y);
    }
};