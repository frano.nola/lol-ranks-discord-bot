const RankService         = require('../services/rank.service');
const MasteryService      = require('../services/mastery.service');
const SpellService        = require('../services/spell.service');
const ChampionService     = require('../services/champion.service');
const SummonerIconService = require('../services/summonerIcon.service');
const RunesService        = require('../services/runes.service');
const SummonerService     = require('../services/summoner.service');
const Spells              = require('./spells');
const Champions           = require('./champions');
const SummonerIcons       = require('./summonerIcons');
const Runes               = require('./runes');
const Mastery             = require('./mastery');
const Assets              = require('./assets');

module.exports = class Ranks {
    rankService;
    summonerService;
    summonerIcons;
    fonts;
    runes;

    constructor(assets, fonts) {
        this.assets          = assets;
        this.fonts           = fonts;
        this.rankService     = new RankService();
        this.summonerService = new SummonerService();
        this.summonerIcons   = new SummonerIcons();
        this.runes           = new Runes();
    }

    setAllRanks(players) {
        let promises = [];

        for (let i = 0; i < players.length; i++) {
            promises.push(this.rankService.getRank(players[i].summonerId).then(res => {
                players[i].rank = res.body;
                i++;
            }));
        }

        return Promise.all(promises);
    }

    async getRanks(summoner) {
        let ranks = {
            "RANKED_SOLO_5x5": {
                "queueType"   : "",
                "wins"        : "",
                "losses"      : "",
                "rank"        : "",
                "tier"        : "",
                "leaguePoints": ""
            },
            "RANKED_FLEX_SR" : {
                "queueType"   : "",
                "wins"        : "",
                "losses"      : "",
                "rank"        : "",
                "tier"        : "",
                "leaguePoints": ""
            },
        };

        for (let i = 0; i < summoner.rank.length; i++)
            ranks[summoner.rank[i].queueType] = summoner.rank[i];

        return ranks;
    }

    getQueueType(queue) {
        switch (queue) {
            case "RANKED_SOLO_5x5":
                return "Solo";
            case "RANKED_FLEX_SR":
                return "Flex";
            default:
                return "3v3";
        }
    }

    placeBanner(image, bannerImage, x, y) {
        bannerImage.resize(208, 105);
        image.blit(bannerImage, 11 + x, 402 + y);
    }

    showRanksImage(channelID, players) {
        console.log("Started viewing");

        const y = 502;
        const x = 222;

        let promises = [];
        let that     = this;

        players.forEach(function (value, i) {
            promises.push(new Promise(function (resolve) {
                Promise.all(
                    [
                        that.summonerService.getSummonerById(value.summonerId),
                        MasteryService.getMastery(value.summonerId, value.championId),
                        that.getRanks(value)
                    ]
                ).then(function (values) {
                    players[i].summonerLevel = values[0].body.summonerLevel;
                    players[i].mastery       = values[1] !== undefined ? values[1].body : undefined;
                    players[i].ranks         = values[2];

                    SpellService.getSpell(value, 'spell1Id').then(
                        image2 => Spells.placeSpell(that.assets.background, that.assets.border, image2, x * (i % (players.length / 2)) + 24, y * (value.teamId / 100 - 1))
                    );
                    SpellService.getSpell(value, 'spell2Id').then(
                        image2 => Spells.placeSpell(that.assets.background, that.assets.border, image2, x * (i % (players.length / 2)) + 170, y * (value.teamId / 100 - 1))
                    );

                    RankService.getBanner(value).then(banner => that.placeBanner(that.assets.background, banner, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1)));

                    return ChampionService.getChampionIcon(value);
                }).then(icon => {
                    Champions.placeChampionIcon(that.assets.background, icon, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                    Assets.placeChampionGradient(that.assets.background, that.assets.gradient, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));

                    let waitAMoment = [];

                    waitAMoment.push(new Promise(function (resolve) {
                        that.summonerIcons.getSummonerIcon(value).then(result => {
                            return SummonerIconService.getSummonerIconSlot(result);
                        }).then(slot => {
                            Assets.placeSummonerIconSlot(that.assets.background, slot, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1), that.assets.mask1);
                            return SummonerIconService.loadSummonerIcon(value);
                        }).then(function (icon) {
                            SummonerIcons.placeSummonerIcon(that.assets.background, icon, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                            that.fonts.setupText(value, that.assets.background, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                            resolve();
                        });
                    }));

                    waitAMoment.push(new Promise(function (resolve) {
                        that.runes.getRuneStyleInfos(value).then(styles => {
                            Runes.placeRuneSlots(that.assets.background, that.assets.runeSlot, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                            return that.runes.getRunes(value, styles);
                        }).then(function (runes) {
                            return RunesService.loadRunes(runes);
                        }).then(function (values) {
                            Runes.placeRunes(that.assets.background, values, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                            resolve();
                        });
                    }));

                    if (players[i].mastery !== undefined)
                        waitAMoment.push(new Promise(function (resolve) {
                            MasteryService.loadChampionMastery(value).then(function (mastery) {
                                Mastery.setChampionMasterySlot(that.assets.background, that.assets.masterySlot, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                                Mastery.setChampionMastery(that.assets.background, mastery, x * (i % (players.length / 2)), y * (value.teamId / 100 - 1));
                                resolve();
                            });
                        }));

                    Promise.all(waitAMoment).then(ignored => resolve());
                }).catch(res => {
                    console.log(res);
                })
            }));
        });

        return Promise.all(promises);
    }
};