const superagent = require('superagent');
const config     = require('../config/config.json');

module.exports = class GameService {
    getGameDetail(id) {
        return superagent
            .get(config.apiUrl + config.urls.gameDetail + id)
            .set("X-Riot-Token", config.api)
            .catch(err => {
                if (err.status !== 404) {
                    console.log(err.message);
                }
            });
    }
};