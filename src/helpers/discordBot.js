const Fonts         = require('./fonts');
const Assets        = require('./assets');
const Game          = require('./game');
const SoundsService = require('../services/sounds.service');
// const SummonerService = require('../services/summoner.service');

const Discord = require('discord.io');
const auth    = require('../config/auth.json');

module.exports = class DiscordBot {
    fonts;
    assets;
    game;
    soundsService;
    summoners = [];
    bot;

    constructor() {
        this.bot = new Discord.Client({
            token  : auth.token,
            autorun: true
        });

        let that           = this;
        this.fonts         = new Fonts();
        this.assets        = new Assets();
        this.game          = new Game();
        this.soundsService = new SoundsService();

        this.bot.on('ready', function (evt) {
            console.log('Connected');
            console.log('Logged in as: ' + that.bot.username + ' - (' + that.bot.id + ')');
            // that.checkUsers();
        });
        this.bot.on('message', function (user, userID, channelID, message, evt) {
            if (message.substring(0, 1) === '!') {
                let args = message.substring(1).split(' ');
                let cmd  = args[0];

                args = args.splice(1);
                switch (cmd) {
                    case 'commands':
                        that.allCommands(channelID);
                        break;
                    case 'sound':
                        that.soundsService.playSomething(userID, args.join(' ') + '.mp3', this, channelID);
                        break;
                    case 'game':
                        that.game.gameInfo(args.join(' '), that.assets, that.fonts, that.bot, channelID);
                        break;
                    // case 'addSummoner':
                    //     SummonerService().getSummonerByName(args.join(' ')).then(res =>
                    //         that.summoners.push(
                    //             {
                    //                 id      : res.id,
                    //                 lastGame: 0
                    //             }
                    //         )
                    //     ).catch(res => console.log("Can't find summoner"));
                    //     break;
                }
            }
        });
    }

    allCommands(channelID) {
        this.bot.sendMessage({
            to     : channelID,
            message: "Available commands: !sound - plays a sound, !game (summoner) - " +
                "shows information about the game player is currently in"
        });
    }

    // checkUsers() {
    //     let that = this;
    //     setInterval(function () {
    //         for (let i = 0; i < that.summoners; i++) {
    //             that.game.gameInfo(that.summoner[i].name, that.assets, that.fonts);
    //         }
    //     }, 5000);
    // }
};