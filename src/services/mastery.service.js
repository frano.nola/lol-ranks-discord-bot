const superagent = require('superagent');
const config     = require('../config/config.json');
const Jimp       = require('jimp');

module.exports = class MasteryService {
    static getMastery(summonerId, championId) {
        return superagent
            .get(config.apiUrl + config.urls.getMastery.bySummoner + summonerId + config.urls.getMastery.andChampion + championId)
            .set("X-Riot-Token", config.api)
            .catch(ignored => {

            });
    }

    static loadChampionMastery(summoner) {
        return Jimp.read("./src/assets/img/mastery/" + summoner.mastery.championLevel + ".png");
    }
};