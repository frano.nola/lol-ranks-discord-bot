const superagent = require('superagent');
const config     = require('../config/config.json');
const Jimp       = require('jimp');

module.exports = class RankService {
    getRank(id) {
        return superagent
            .get(config.apiUrl + config.urls.getRank + id)
            .set("X-Riot-Token", config.api)
            .catch(err => {
                console.log(err.message);
            });
    }

    static getBanner(summoner) {
        if (summoner.ranks["RANKED_SOLO_5x5"] && summoner.ranks["RANKED_SOLO_5x5"].tier !== "")
            return Jimp.read("./src/assets/img/banner/" + summoner.ranks["RANKED_SOLO_5x5"].tier.toLowerCase() + ".png");
        else if (summoner.ranks["RANKED_FLEX_SR"] && summoner.ranks["RANKED_FLEX_SR"].tier !== "")
            return Jimp.read("./src/assets/img/banner/" + summoner.ranks["RANKED_FLEX_SR"].tier.toLowerCase() + ".png");
        else
            return Jimp.read("./src/assets/img/banner/unranked.png");
    }
};
