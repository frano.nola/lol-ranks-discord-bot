module.exports = class Mastery {
    static setChampionMastery(image, mastery, x, y) {
        mastery.resize(63, 61);
        image.blit(mastery, 143 + x, 70 + y);
    }

    static setChampionMasterySlot(image, slot, x, y) {
        image.blit(slot, 141 + x, 45 + y);
    }
};