const Jimp = require('jimp');

module.exports = class FontsService {
    static loadAllFonts() {
        return Promise.all(
            [
                Jimp.loadFont("./src/assets/font/lol12.fnt"),
                Jimp.loadFont("./src/assets/font/lol14.fnt"),
                Jimp.loadFont("./src/assets/font/lol14Gold.fnt"),
                Jimp.loadFont("./src/assets/font/lol16.fnt"),
                Jimp.loadFont("./src/assets/font/lol16Gold.fnt")
            ]
        );
    }
};
