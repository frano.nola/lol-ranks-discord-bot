const SummonerService = require('../services/summoner.service');
const GameService     = require('../services/game.service');
const Ranks           = require('../helpers/ranks');

module.exports = class Game {
    gameInfo(name, assets, fonts, bot, channelID) {
        let ranks = new Ranks(assets, fonts);
        let players;

        new SummonerService().getSummonerByName(name).then(res => {
            return new GameService().getGameDetail(res.body.id);
        }).then(res => {
            players = res.body.participants;
            return ranks.setAllRanks(players);
        }).then(function () {
            return ranks.showRanksImage(channelID, players)
        }).then(ignored => assets.background.writeAsync("./src/assets/img/rank.jpg").then(function () {
            console.log("COMPLETED");
            bot.uploadFile({
                to  : channelID,
                file: "src/assets/img/rank.jpg"
            });
        })).catch(ignored => {
            bot.sendMessage({
                to     : channelID,
                message: "Can't find a game with " + name + ", player might not be in the game"
            });
        });
    }
};
