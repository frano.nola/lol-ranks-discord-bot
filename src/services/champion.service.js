const Jimp     = require('jimp');
const config   = require('../config/config.json');
const champion = require(config.infos.champion);

module.exports = class ChampionService {
    static getChampionIcon(summoner) {
        for (let dataKey in champion.data) {
            if (+champion.data[dataKey].key === summoner.championId) {
                return Jimp.read('src/' + config.images.championTile + champion.data[dataKey].id + "_0.jpg");
            }
        }
    }
};