var fs = require('fs');

module.exports = class SoundService {
    getAllSounds() {
        let message = "";
        fs.readdirSync("./src/assets/sounds/").forEach(file => {
            if (file.endsWith(".mp3"))
                message += file.slice(0, -4) + ", ";
        });

        return message.slice(0, -2);
    }

    playSomething(userID, fileName, bot, channelID) {
        if (fileName !== ".mp3")
            if (fs.existsSync('./src/assets/sounds/' + fileName) === true)
                loop:
                    for (let key in bot.channels) {
                        for (let key2 in bot.channels[key].members) {
                            if (key2 === userID) {
                                bot.joinVoiceChannel(key, function (error, events) {
                                    if (error) return console.error(error);

                                    bot.getAudioContext(key, function (error, stream) {
                                        //Once again, check to see if any errors exist
                                        if (error) return console.error(error);

                                        //Create a stream to your file and pipe it to the stream
                                        //Without {end: false}, it would close up the stream, so make sure to include that.
                                        fs.createReadStream('./src/assets/sounds/' + fileName).pipe(stream, {end: false});

                                        //The stream fires `done` when it's got nothing else to send to Discord.
                                        stream.on('done', function () {
                                            bot.leaveVoiceChannel(key, function (error, stream) {

                                            })
                                        });

                                    });
                                });
                                break loop;
                            }
                        }
                    }
            else {
                bot.sendMessage({
                    to     : channelID,
                    message: "Can't find that sound. Available sounds: " + this.getAllSounds()
                });
            }
        else {
            bot.sendMessage({
                to     : channelID,
                message: "Please enter a sound. Available sounds: " + this.getAllSounds()
            });
        }
    }
};
