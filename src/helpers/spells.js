module.exports = class Spells {
    static placeSpell(image, border, spellImage, x, y) {
        spellImage.resize(30, 30);
        image.blit(spellImage, 3 + x, 258 + y);
        image.blit(border, x, 255 + y);
    }
};