const Jimp   = require('jimp');
const config = require('../config/config.json');
const icons  = require(config.infos.summoners);

module.exports = class SpellService {
    static getSpell(summoner, type) {
        for (let dataKey in icons.data) {
            if (+icons.data[dataKey].key === summoner[type]) {
                return Jimp.read('src/' + config.images.spell + icons.data[dataKey].image.full);
            }
        }
    }
};