const superagent = require('superagent');
const config     = require('../config/config.json');

module.exports = class SummonerService {
    getSummonerByName(name) {
        return superagent
            .get(config.apiUrl + config.urls.getSummonerByName + name)
            .set("X-Riot-Token", config.api)
            .catch(err => {
                console.log(err.message);
            });
    }

    getSummonerById(id) {
        return superagent
            .get(config.apiUrl + config.urls.getSummonerById + id)
            .set("X-Riot-Token", config.api)
            .catch(err => {
                console.log(err.message);
            });
    }
};
