const Jimp          = require('jimp');

module.exports = class AssetsService {
    static loadAssets() {
        return Promise.all(
            [
                Jimp.read("./src/assets/img/bcg.jpg"),
                Jimp.read("./src/assets/img/border.png"),
                Jimp.read("./src/assets/img/mask.png"),
                Jimp.read("./src/assets/img/mask.png"),
                Jimp.read("./src/assets/img/runeSlot.png"),
                Jimp.read("./src/assets/img/masterySlot.png"),
                Jimp.read("./src/assets/img/gradient.png")
            ]
        );
    }
};