module.exports = class Champions {
    static placeChampionIcon(image, championIcon, x, y) {
        championIcon.resize(198, 198);
        image.blit(championIcon, 16 + x, 45 + y);
    }
};