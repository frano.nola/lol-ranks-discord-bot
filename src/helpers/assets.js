const AssetsService = require('../services/assets.service');

module.exports = class Assets {
    background;
    border;
    mask1;
    mask2;
    runeSlot;
    masterySlot;
    gradient;

    constructor() {
        AssetsService.loadAssets().then(images => {
            this.setAssets(images);
            this.resizeImages();
        });
    }

    setAssets(images) {
        this.background  = images[0];
        this.border      = images[1];
        this.mask1       = images[2];
        this.mask2       = images[3];
        this.runeSlot    = images[4];
        this.masterySlot = images[5];
        this.gradient    = images[6];
    }

    resizeImages() {
        this.border.resize(36, 36);
        this.mask1.resize(90, 90);
        this.mask2.resize(31, 31);
        this.runeSlot.resize(35, 35);
        this.masterySlot.resize(69, 137);
    }

    static placeSummonerIconSlot(image, slot, x, y, mask) {
        slot.resize(90, 90);
        slot.mask(mask, 0, 0);
        image.blit(slot, 69 + x, 191 + y);
    }

    static placeChampionGradient(image, gradient, x, y) {
        image.blit(gradient, 16 + x, 45 + y);
    }
};